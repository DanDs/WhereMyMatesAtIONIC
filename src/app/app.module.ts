import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RegisterPage } from "../pages/register/register";
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule, AngularFireAuth} from 'angularfire2/auth';
import {AngularFireDatabaseModule, AngularFireDatabase} from 'angularfire2/database';
import { LoginPage } from "../pages/login/login";
import { GoogleMaps } from "@ionic-native/google-maps";
import { MessageServiceProvider } from '../providers/message-service/message-service';
import { MyplacesServiceProvider } from '../providers/myplaces-service/myplaces-service';
import { ContactServiceProvider } from '../providers/contact-service/contact-service';
import { ExitPage } from "../pages/exit/exit";

export const configFirebase = {

  apiKey: "AIzaSyB6REcUFgTsmG_SxoNKTpB0UcMqeDzfXIc",
  authDomain: "wheremymatesationic.firebaseapp.com",
  databaseURL: "https://wheremymatesationic.firebaseio.com",
  projectId: "wheremymatesationic",
  storageBucket: "",
  messagingSenderId: "481732955712"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    ExitPage    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    AngularFireModule.initializeApp(configFirebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    ExitPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpClient,
    AngularFireDatabase,
    AngularFireAuth,
    AngularFireDatabase,
    AngularFireAuth,
    GoogleMaps,
    MessageServiceProvider,
    MyplacesServiceProvider,
    ContactServiceProvider
  ]
})
export class AppModule {}
