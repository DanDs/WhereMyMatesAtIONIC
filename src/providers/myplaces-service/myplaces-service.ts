import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Place } from "../../models/place";
import { AngularFireList, AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from "rxjs/Observable";

/*
  Generated class for the MyplacesServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyplacesServiceProvider {

  myplaces: AngularFireList<Place>; 
  
  constructor(public http: HttpClient, 
    public myplacesdb: AngularFireDatabase,
    public afAuth: AngularFireAuth ) {
    this.myplaces = myplacesdb.list('/myplaces/'+afAuth.auth.currentUser.uid);
    console.log(this.myplaces);
  }

  getMyPlaces(): Observable<Place[]>{    
    return this.myplaces.valueChanges();
  }

  newPlace(place: Place){    
    var newPostKey = this.myplacesdb.database.ref().child('myplaces'+this.afAuth.auth.currentUser.uid).push().key;
    //place.$key = newPostKey;
    console.log(newPostKey);
    this.myplaces.push(place);    
  }

  deletePlace(place: Place): void {
    this.myplacesdb.object('/myplaces/'+this.afAuth.auth.currentUser.uid).remove();    
    console.log('borrado');
  }
  

}
