export class Place {
    
      $key: String;
      public title: String;
      public user: String;
      public latitude: number;
      public longitude: number;
    
      constructor() {
      }
      
    }