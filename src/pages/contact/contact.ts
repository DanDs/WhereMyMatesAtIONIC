import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Contact } from "../../models/contact";
import { ContactServiceProvider } from "../../providers/contact-service/contact-service";
import { ExitPage } from "../exit/exit";

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  contacts: Contact[];
  
  constructor(public navCtrl: NavController, public contactService: ContactServiceProvider, public alertCtrl: AlertController) {
    let suscriptor =contactService.getContacts().subscribe(data => {
      this.contacts = data;
    });
  }

  addContact(){
    let prompt = this.alertCtrl.create({
      title: 'Contacts',
      message: "Adding a New Contact",
      inputs: [
        {
          name: 'name',
          placeholder: 'Name'
        },
        {
          name: 'email',
          placeholder: 'Email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            this.contactService.addContact(data);  
          }
        }
      ]
    });
    prompt.present();
  }

  goToChat(contact: Contact){
    this.navCtrl.push('ChatPage', {'contact': contact});    
  }

  exit(){
    this.navCtrl.push(ExitPage);
  }
  
}
