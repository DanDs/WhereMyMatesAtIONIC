import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  GoogleMapsAnimation,
  MyLocation
 } from '@ionic-native/google-maps';
import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Place } from "../../models/place";
import { ExitPage } from "../exit/exit";
import { MyplacesServiceProvider } from "../../providers/myplaces-service/myplaces-service";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  mapReady: boolean = false;
  map: GoogleMap;
  marker: Marker;
  markers: Place[];
  place: Place = new Place();
  constructor(public navCtrl: NavController, public googleMaps: GoogleMaps,  public toastCtrl: ToastController, public mymarkers: MyplacesServiceProvider) {
    let suscriptor =mymarkers.getMyPlaces().subscribe(data => {
      this.markers = data;
   });
  }

  ionViewDidLoad() {
    this.loadMap();
  }

  loadMap() {
    
        let mapOptions: GoogleMapOptions = {
          camera: {
            target: {
              lat: this.place.latitude,
              lng: this.place.longitude
            },
            zoom: 18,
            tilt: 30
          }
        };
    
        this.map = GoogleMaps.create('map_canvas', mapOptions);
         
      
        this.map.one(GoogleMapsEvent.MAP_READY)
          .then(() => {
            console.log('Map is ready!');    
            
            this.map.addMarker({
                title: 'Pin',
                icon: 'blue',
                animation: 'DROP',
                position: {
                  lat: this.place.latitude,
                  lng: this.place.longitude
                },
                draggable: true
              })
              .then(marker => {
                this.marker = marker;
    
                marker.on(GoogleMapsEvent.MARKER_DRAG_END)
                .subscribe((marker) => {
                  console.log(marker);
                  this.place.latitude = marker.getPosition().lat;
                  this.place.longitude = marker.getPosition().lng;
                });
    
                marker.on(GoogleMapsEvent.MARKER_CLICK)
                  .subscribe(() => {
                    alert('clicked');
                  });
              });
    
          });
      }

  onButtonClick() {
    if (!this.mapReady) {
      this.showToast('The map is not ready yet. Please try again.');
      return;
    }
    this.map.clear();

    this.map.getMyLocation()
      .then((location: MyLocation) => {
        console.log(JSON.stringify(location, null ,2));
        
        return this.map.animateCamera({
          target: location.latLng,
          zoom: 17,
          tilt: 30
        }).then(() => {
          
          return this.map.addMarker({
            title: 'Function not tested',
            snippet: 'But it seems to work',
            position: location.latLng,
            animation: GoogleMapsAnimation.BOUNCE
          });
        })
      }).then((marker: Marker) => {        
        marker.showInfoWindow();
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          this.showToast('clicked!');
        });
      });
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });

    toast.present(toast);
  }

  exit(){
    this.navCtrl.push(ExitPage);
  } 
}
